package com.assignment.studentsortingapp.presentation.controller;

import com.assignment.studentsortingapp.application.service.StudentService;
import com.assignment.studentsortingapp.application.sorting.SortingAlgorithm;
import com.assignment.studentsortingapp.domain.model.Student;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StudentControllerTests {

    @Test
    void testSortStudents() throws Exception {
        StudentService studentService = mock(StudentService.class);

        List<Student> sortedStudents = Arrays.asList(
                new Student("Student1", 8.5),
                new Student("Student2", 6.5),
                new Student("Student3", 5.0)
        );

        when(studentService.sortStudents(Mockito.any(), Mockito.any()))
                .thenReturn(sortedStudents);

        StudentController studentController = new StudentController(studentService);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();

        mockMvc.perform(MockMvcRequestBuilders.multipart("/sort")
                        .file(new MockMultipartFile("file", "students.csv", "text/csv", "file content".getBytes()))
                        .param("algorithm", SortingAlgorithm.BUBBLE_SORT.name()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("students"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("downloadAvailable"))
                .andExpect(MockMvcResultMatchers.view().name("sort_result"));
    }

    @Test
    void testDownloadCSV() throws Exception {
        StudentService studentService = mock(StudentService.class);

        List<Student> students = Arrays.asList(
                new Student("Student1", 8.5),
                new Student("Student2", 6.5),
                new Student("Student3", 5.0)
        );

        StudentController studentController = new StudentController(studentService);

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();

        when(studentService.generateCsvContent(Mockito.anyList()))
                .thenReturn("csvContent");

        mockMvc.perform(MockMvcRequestBuilders.get("/download")
                        .sessionAttr("students", students))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("csvContent"))
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=sorted_students.csv"))
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_TYPE, "application/csv"))
                .andExpect(MockMvcResultMatchers.header().string(HttpHeaders.CONTENT_LENGTH, "10"));
    }
}


