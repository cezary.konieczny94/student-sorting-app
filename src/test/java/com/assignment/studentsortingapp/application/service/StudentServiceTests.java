package com.assignment.studentsortingapp.application.service;

import com.assignment.studentsortingapp.application.csv.StudentCsvProcessor;
import com.assignment.studentsortingapp.application.sorting.SortingAlgorithm;
import com.assignment.studentsortingapp.domain.model.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class StudentServiceTests {

    @Mock
    private StudentCsvProcessor csvProcessor;

    private StudentService studentService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        studentService = new StudentService(csvProcessor);
    }

    @Test
    public void testSortStudents() throws IOException {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Student1", 8.5));
        students.add(new Student("Student2", 6.5));
        students.add(new Student("Student3", 5.0));

        // Create the CSV file programmatically
        Path csvFilePath = Files.createTempFile("students", ".csv");
        String csvContent = generateCsvContent(students);
        Files.write(csvFilePath, csvContent.getBytes());

        MultipartFile file = new MockMultipartFile("students.csv", csvFilePath.getFileName().toString(),
                "text/csv", Files.readAllBytes(csvFilePath));

        when(csvProcessor.parse(any(MultipartFile.class))).thenReturn(students);

        List<Student> sortedStudents = studentService.sortStudents(file, SortingAlgorithm.BUBBLE_SORT);

        List<Student> expectedSortedStudents = Arrays.asList(
                new Student("Student3", 5.0),
                new Student("Student2", 6.5),
                new Student("Student1", 8.5)
        );
        assertEquals(expectedSortedStudents, sortedStudents);
    }

    private String generateCsvContent(List<Student> students) {
        StringBuilder csvContent = new StringBuilder();
        for (Student student : students) {
            csvContent.append(student.getName()).append(",").append(student.getScore()).append("\n");
        }
        return csvContent.toString();
    }



    @Test
    public void testGenerateCsvContent() {
        List<Student> students = Arrays.asList(
                new Student("Student1", 8.5),
                new Student("Student2", 6.5),
                new Student("Student3", 5.0)
        );

        String expectedCsvContent = "Student1,8.5\nStudent2,6.5\nStudent3,5.0";

        when(csvProcessor.generateCsvContent(any())).thenReturn(expectedCsvContent);

        String csvContent = studentService.generateCsvContent(students);

        assertEquals(expectedCsvContent, csvContent);
    }

}
