package com.assignment.studentsortingapp.domain.infrastructure.exception.handlers;

import com.assignment.studentsortingapp.domain.infrastructure.exception.CsvPrintingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CsvProcessingExceptionHandler {

    @ExceptionHandler(CsvPrintingException.class)
    public ResponseEntity<String> handleCsvProcessingException(CsvPrintingException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

}
