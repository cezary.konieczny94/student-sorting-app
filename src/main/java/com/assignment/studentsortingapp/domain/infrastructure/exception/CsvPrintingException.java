package com.assignment.studentsortingapp.domain.infrastructure.exception;

public class CsvPrintingException extends RuntimeException {

    public CsvPrintingException(String message, Throwable cause) {
        super(message, cause);
    }

}
