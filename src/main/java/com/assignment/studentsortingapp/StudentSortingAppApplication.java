package com.assignment.studentsortingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentSortingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentSortingAppApplication.class, args);
	}

}
