package com.assignment.studentsortingapp.application.service;

import com.assignment.studentsortingapp.application.csv.StudentCsvProcessor;
import com.assignment.studentsortingapp.application.sorting.CollectionSorter;
import com.assignment.studentsortingapp.application.sorting.SortingAlgorithm;
import com.assignment.studentsortingapp.domain.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
public class StudentService {
    private final StudentCsvProcessor csvProcessor;
    private static final Comparator<Student> scoreComparator = Comparator.comparing(Student::getScore);

    public StudentService(StudentCsvProcessor csvProcessor) {
        this.csvProcessor = csvProcessor;
    }
    public List<Student> sortStudents(MultipartFile file, SortingAlgorithm algorithm) {
        List<Student> students = csvProcessor.parse(file);
        CollectionSorter.sort(algorithm, students, scoreComparator);
        return students;
    }


    public String generateCsvContent(List<Student> students) {
        return csvProcessor.generateCsvContent(students);
    }


}
