package com.assignment.studentsortingapp.application.sorting;

public enum SortingAlgorithm {
    BUBBLE_SORT("Bubble sort"),
    HEAP_SORT("Heap sort"),
    MERGE_SORT("Merge sort");

    public final String label;

    SortingAlgorithm(String label) {
        this.label = label;
    }
}
