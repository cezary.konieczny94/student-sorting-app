package com.assignment.studentsortingapp.application.sorting;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CollectionSorter {

    public static <T> void sort(SortingAlgorithm algorithm, Collection<T> collection, Comparator<? super T> comparator) {
        long startTime = System.nanoTime();
        switch (algorithm) {
            case BUBBLE_SORT -> bubbleSort(collection, comparator);
            case HEAP_SORT -> heapSort(collection, comparator);
            case MERGE_SORT -> mergeSort(collection, comparator);
        }
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;

        String timeUnit = "ns";
        if (elapsedTime >= 1000){
            elapsedTime = TimeUnit.NANOSECONDS.toMillis(elapsedTime);
            timeUnit = "ms";
        }
        log.info("Number of records: " + collection.size());
        log.info(algorithm.label + " Sorting time: " + elapsedTime + " " + timeUnit);
    }

    private static <T> void bubbleSort(Collection<T> collection, Comparator<? super T> comparator) {
        T[] array = (T[]) collection.toArray();
        int n = array.length;
        boolean swapped;
        do {
            swapped = false;
            for (int i = 1; i < n; i++) {
                if (comparator.compare(array[i - 1], array[i]) > 0) {
                    swap(array, i - 1, i);
                    swapped = true;
                }
            }
            n--;
        } while (swapped);
        updateCollection(collection, array);
    }

    private static <T> void heapSort(Collection<T> collection, Comparator<? super T> comparator) {
        T[] array = (T[]) collection.toArray();
        int n = array.length;

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(array, n, i, comparator);
        }

        for (int i = n - 1; i > 0; i--) {
            swap(array, 0, i);
            heapify(array, i, 0, comparator);
        }

        updateCollection(collection, array);
    }

    private static <T> void mergeSort(Collection<T> collection, Comparator<? super T> comparator) {
        T[] array = (T[]) collection.toArray();
        mergeSort(array, 0, array.length - 1, comparator);
        updateCollection(collection, array);
    }

    private static <T> void mergeSort(T[] array, int left, int right, Comparator<? super T> comparator) {
        if (left < right) {
            int middle = left + (right - left) / 2;
            mergeSort(array, left, middle, comparator);
            mergeSort(array, middle + 1, right, comparator);
            merge(array, left, middle, right, comparator);
        }
    }

    private static <T> void merge(T[] array, int left, int middle, int right, Comparator<? super T> comparator) {
        int n1 = middle - left + 1;
        int n2 = right - middle;

        T[] leftArray = (T[]) new Object[n1];
        T[] rightArray = (T[]) new Object[n2];

        System.arraycopy(array, left, leftArray, 0, n1);
        System.arraycopy(array, middle + 1, rightArray, 0, n2);

        int i = 0;
        int j = 0;
        int k = left;

        while (i < n1 && j < n2) {
            if (comparator.compare(leftArray[i], rightArray[j]) <= 0) {
                array[k] = leftArray[i];
                i++;
            } else {
                array[k] = rightArray[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            array[k] = leftArray[i];
            i++;
            k++;
        }

        while (j < n2) {
            array[k] = rightArray[j];
            j++;
            k++;
        }
    }

    private static <T> void heapify(T[] array, int n, int i, Comparator<? super T> comparator) {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && comparator.compare(array[left], array[largest]) > 0) {
            largest = left;
        }

        if (right < n && comparator.compare(array[right], array[largest]) > 0) {
            largest = right;
        }

        if (largest != i) {
            swap(array, i, largest);
            heapify(array, n, largest, comparator);
        }
    }

    private static <T> void swap(T[] array, int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    private static <T> void updateCollection(Collection<T> collection, T[] array) {
        collection.clear();
        collection.addAll(Arrays.asList(array));
    }

}
