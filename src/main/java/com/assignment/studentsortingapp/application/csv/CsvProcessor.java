package com.assignment.studentsortingapp.application.csv;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CsvProcessor<T> {
    List<T> parse(MultipartFile file);

    String generateCsvContent(List<T> objects);

}
