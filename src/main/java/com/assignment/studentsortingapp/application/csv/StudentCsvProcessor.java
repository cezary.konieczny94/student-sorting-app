package com.assignment.studentsortingapp.application.csv;

import com.assignment.studentsortingapp.domain.infrastructure.exception.CsvParsingException;
import com.assignment.studentsortingapp.domain.infrastructure.exception.CsvPrintingException;
import com.assignment.studentsortingapp.domain.model.Student;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Component
public class StudentCsvProcessor implements CsvProcessor<Student>{

    private final CSVFormat csvFormat;

    public StudentCsvProcessor() {
        csvFormat = CSVFormat.DEFAULT.builder().setHeader("Name", "Score").build();
    }

    public List<Student> parse(MultipartFile file) {
        try (Reader reader = new InputStreamReader(file.getInputStream())) {
            CSVParser csvParser = csvFormat.parse(reader);

            List<Student> students = new ArrayList<>();

            for (CSVRecord csvRecord : csvParser) {
                String name = csvRecord.get("Name");
                double score = Double.parseDouble(csvRecord.get("Score"));
                students.add(new Student(name, score));
            }

            return students;
        } catch (IOException e) {
            throw new CsvParsingException("Error reading student data from CSV", e);
        }
    }

    public String generateCsvContent(List<Student> students) {
        try (StringWriter stringWriter = new StringWriter()) {
            CSVPrinter csvPrinter = csvFormat.print(stringWriter);

            for (Student student : students) {
                csvPrinter.printRecord(student.getName(), student.getScore());
            }

            csvPrinter.flush();

            return stringWriter.toString();
        } catch (IOException e) {
            throw new CsvPrintingException("Error generating CSV content", e);
        }
    }
}

