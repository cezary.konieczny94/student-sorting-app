package com.assignment.studentsortingapp.presentation.controller;

import com.assignment.studentsortingapp.application.service.StudentService;
import com.assignment.studentsortingapp.application.sorting.SortingAlgorithm;
import com.assignment.studentsortingapp.domain.model.Student;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@SessionAttributes("students")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @PostMapping("/sort")
    public String sortStudents(@RequestParam("file") MultipartFile file,
                               @RequestParam("algorithm") SortingAlgorithm algorithm,
                               Model model) {
        List<Student> sortedStudents = studentService.sortStudents(file, algorithm);
        model.addAttribute("students", sortedStudents);
        model.addAttribute("downloadAvailable", sortedStudents != null && !sortedStudents.isEmpty());
        return "sort_result";
    }


    @GetMapping("/download")
    public ResponseEntity<Resource> downloadCSV(@ModelAttribute("students") List<Student> students) {
        String csvContent = studentService.generateCsvContent(students);
        ByteArrayResource resource = new ByteArrayResource(csvContent.getBytes());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=sorted_students.csv")
                .contentType(MediaType.parseMediaType("application/csv"))
                .contentLength(csvContent.getBytes().length)
                .body(resource);
    }
}




